//
//  PokedexViewController.swift
//  Marvel_CocoaPods_JG
//
//  Created by Jose Gonzalez on 29/11/17.
//  Copyright © 2017 Jose Gonzalez. All rights reserved.
//

import UIKit

class PokedexViewController: UIViewController,UITableViewDelegate, UITableViewDataSource, PokemonServiceDelegate {

    @IBOutlet weak var pokedexTableView: UITableView!
    var pokemonArray:[Pokemon] = []
    var pokemonIndex:Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let service = PokemonService()
        service.delegate = self
        service.downloadPokemons()

    }

    
    func get20FirstPokemons(pokemons: [Pokemon]) {
        print(pokemons)
        
        pokemonArray = pokemons
        pokedexTableView.reloadData()
    }
    
    
    
    
    
    //MARK:- TableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        switch section {
//        case 0:
//            return 5
//        default:
//            return 10
//        }

        return pokemonArray.count
    
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
       
        pokemonIndex = indexPath.row
        return indexPath
        }
        
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "pokemonCell") as! PokemonTableViewCell
        
        
        //cell.textLabel?.text = pokemonArray[indexPath.row].name
        
        cell.detailTextLabel?.textColor = UIColor.white
        cell.fillDAta(pokemon: pokemonArray[indexPath.row])
        //cell.contentView.backgroundColor = UIColor.init(displayP3Red: 255.0, green: 59.0, blue: 48.0, alpha: 1.0)
        
        
//        switch indexPath.section {
//        case 0:
//                cell.contentView.backgroundColor = UIColor.blue
//        default:
//              cell.contentView.backgroundColor = UIColor.red
//        }
//
      
 
        
        return cell
    }
 
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String {
       
        switch section {
        case 0:
            return "Sección 1"
        default:
            return "Sección 2"
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let pokemonDetail = segue.destination as! DetIailViewController
        
        pokemonDetail.pokemon = pokemonArray[pokemonIndex!]
        
    }
    
    
    
    }
    
    


