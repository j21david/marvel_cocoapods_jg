//
//  PokemonTableViewCell.swift
//  Marvel_CocoaPods_JG
//
//  Created by Jose Gonzalez on 3/1/18.
//  Copyright © 2018 Jose Gonzalez. All rights reserved.
//

import UIKit

class PokemonTableViewCell: UITableViewCell {
    
    
    
    @IBOutlet weak var namePokemonLabel: UILabel!
    @IBOutlet weak var heightPokemonLabel: UILabel!
    @IBOutlet weak var weightPokemonLabel: UILabel!
    
    var pokemon:Pokemon?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    func fillDAta(pokemon:Pokemon){
        
        namePokemonLabel.text = pokemon.name
        heightPokemonLabel.text = "\(pokemon.height ?? 0)"
        weightPokemonLabel.text = "\(pokemon.weight ?? 0)"
        
    }
    
    
    
    
}
