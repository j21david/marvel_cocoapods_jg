//
//  ViewController.swift
//  Marvel_CocoaPods_JG
//
//  Created by Jose Gonzalez on 28/11/17.
//  Copyright © 2017 Jose Gonzalez. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
   
    @IBOutlet weak var heightLabel: UILabel!
    
    
    @IBOutlet weak var weightLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.downloadImage("https://comicvine.gamespot.com/api/image/scale_medium/1315497-61_klaws_of_the_panther_1.jpg", inView: imageView)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func consultarButtonPressed(_ sender: Any) {
        
        let pkId = arc4random_uniform(250) + 1
        
        Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(pkId)").responseObject {( response: DataResponse<Pokemon>) in
        
            let pokemon = response.result.value
            DispatchQueue.main.async {
                self.nameLabel.text = pokemon?.name ?? " "
                 self.heightLabel.text = "\(pokemon?.height ?? 0)"
                 self.weightLabel.text = "\(pokemon?.weight ?? 0)"
            }
            
            
        }
            
            

    
}
    func downloadImage(_ uri : String, inView: UIImageView){
        
        let url = URL(string: uri)
        
        let task = URLSession.shared.dataTask(with: url!) {responseData,response,error in
            if error == nil{
                if let data = responseData {
                    
                    DispatchQueue.main.async {
                        inView.image = UIImage(data: data)
                    }
                    
                }else {
                    print("no data")
                }
            }else{
                print(error)
            }
        }
        
        task.resume()
        
    }

    
    
    
    

}
