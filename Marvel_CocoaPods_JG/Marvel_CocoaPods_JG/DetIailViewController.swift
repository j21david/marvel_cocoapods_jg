//
//  DetIailViewController.swift
//  Marvel_CocoaPods_JG
//
//  Created by Jose Gonzalez on 3/1/18.
//  Copyright © 2018 Jose Gonzalez. All rights reserved.
//

import UIKit

class DetIailViewController: UIViewController {

    @IBOutlet weak var pokemonImage: UIImageView!
    
    var pokemon:Pokemon?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }


    override func viewWillAppear(_ animated: Bool) {
        print(self.pokemon?.name)
        
        self.title = pokemon?.name
        
        let pkService = PokemonService()
        
        pkService.getPokemonImage(id:(pokemon?.id)!){
            (pkImage) in
            self.pokemonImage.image = pkImage
        }
        
        
    }

}
