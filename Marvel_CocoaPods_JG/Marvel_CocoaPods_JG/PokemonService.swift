//
//  PokemonService.swift
//  Marvel_CocoaPods_JG
//
//  Created by Jose Gonzalez on 2/1/18.
//  Copyright © 2018 Jose Gonzalez. All rights reserved.
//

import Foundation
import AlamofireObjectMapper
import Alamofire
import AlamofireImage

protocol PokemonServiceDelegate {
    
    func get20FirstPokemons (pokemons:[Pokemon])
    
    
}


class PokemonService {
    
    var delegate:PokemonServiceDelegate?
    
    func downloadPokemons(){
        var pokemonArray:[Pokemon] = []
        let dpGR = DispatchGroup()
        
        for i in 1...3{
        
            
            dpGR.enter()
            
            Alamofire.request("https://pokeapi.co/api/v2/pokemon/\(i)").responseObject {( response: DataResponse<Pokemon>) in
                
                let pokemon = response.result.value
            
                pokemonArray.append(pokemon!)
        
    
                
                dpGR.leave()
                
        }
        
         
    }
        dpGR.notify(queue: .main){
            

            let sortedArray = pokemonArray.sorted(by: {$0.id! < $1.id!})
            self.delegate?.get20FirstPokemons(pokemons: sortedArray)
        }
        
    
}
    

        func getPokemonImage(id:Int, completion:@escaping (UIImage) -> ()){
        
            Alamofire.request("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/\(id).png").responseImage { response in
                debugPrint(response)
                
                
                if let image = response.result.value {
                    completion(image)
                }
            }
            
            
        }
    
    
    
    
    
    
    
    
    
}
